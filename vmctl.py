#!/usr/bin/env python

from collections.abc import Mapping
import os, sys, getopt, yaml, re, ipaddress, copy, shutil
from os.path import join
from os import path
import contextlib, concurrent.futures
from contextlib import contextmanager
import subprocess

@contextmanager
def working_directory(path):
    """
    A context manager which changes the working directory to the given
    path, and then changes it back to its previous value on exit.
    Usage:
    > # Do something in original directory
    > with working_directory('/my/new/path'):
    >     # Do something in new directory
    > # Back to old directory
    """

    prev_cwd = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prev_cwd)


def replace_matches(placeholders, count, value_str, matches):
    """
    Replace the placeholders with matching value
    """
    if matches == None:
        return
    for match in matches:
        try:
            placeholder = placeholders[match[2:-1]]
        except KeyError as e:
            raise e
        
        if placeholder['type'] == 'constant':
            value_str = value_str.replace(match, placeholder['value'])

        elif placeholder['type'] == 'int-incr':
            incr = placeholder['increment'] if 'increment' in placeholder else 1
            value = placeholder['value'] if 'value' in placeholder else 1
            value_str =  value_str.replace(match, str(value + (count * incr)))

        elif placeholder['type'] == 'ipv6-incr':
            incr = placeholder['increment'] if 'increment' in placeholder else 1
            if 'value' not in placeholder:
                print ('Ipv6 address required for this operation')
                return
            value_str = value_str.replace(match, str(ipaddress.IPv6Address(placeholder['value']) + (incr * count)))
        
        elif placeholder['type'] == 'ipv4-incr':
            incr = placeholder['increment'] if 'increment' in placeholder else 1
            if 'value' not in placeholder:
                print ('Ipv4 address required for this operation')
                return
            value_str = value_str.replace(match, str(ipaddress.IPv4Address(placeholder['value']) + (incr * count)))

        else:
            pass

    return value_str


def find_placeholders(value_str):
    """
    Checks the string for placeholders
    """
    try:
        return re.findall('\$\{.+?\}', value_str)
    except TypeError as e:
        raise e


def generate_template(placeholders, count, template):
    """
    This function iterates a dictionary or a list to update all placeholders
    """
    if isinstance(template, Mapping):
        template_items = template.items()
    elif isinstance(template, list):
        template_items = enumerate(template)
    else:
        raise TypeError("template argument should be of type dictionary or list")
    
    for key, value in template_items:
        
        if isinstance(value, (Mapping, list)):
            generate_template(placeholders, count, value)
        elif isinstance(value, str):
            try:
                template[key] = replace_matches(placeholders, count, value, find_placeholders(value))
            except (KeyError, TypeError) as e:
                raise e


def generate_config(deployment_name, count_num, host_index, host, model_templates, placeholders, config_dir=None):
    """
    Generates a single config for the host
    """
    if config_dir is None:
        config_dir = ''

    uniqueName = replace_matches(host['placeholders'], count_num, host['uniqueName'], find_placeholders(host['uniqueName']))
    for config_index, config in enumerate(host['configs']):
        
        try:                    
            output_template = copy.deepcopy(model_templates[config_index])

            generate_template(placeholders[config_index], count_num, output_template)

            with open(os.path.join(config_dir, config['type'] + "-" + uniqueName + ".cfg"), 'w') as f:
                if config['type'] == 'cloud-config':
                    f.write("#cloud-config\n")
                f.write(yaml.dump(output_template))

        except TypeError as e:
            print(e)
            return
        except KeyError as e:
            print("{} placeholder is not present in the {} at index {} in config index {}.\nSkipping config".format(e, host['name'], host_index, config_index + 1))
            return

    with open(os.path.join(config_dir, deployment_name + "-" + host['type'] + ".txt"), 'a') as f:
        f.write("{}\n".format(uniqueName))
    

    if host['type'] == 'kvm':
        subprocess.run(['cloud-localds', '-v', '--network-config', os.path.join(config_dir, 'network-config-' + uniqueName + '.cfg'), os.path.join(config_dir, uniqueName + '.seed'), os.path.join(config_dir, 'cloud-config-' + uniqueName + '.cfg')])

    return uniqueName


def generate_host(host_index, host, templates, deployment_name):
    """
    Generates all configs for the host
    """
    host_number = host_index + 1

    try:
        host_name = host['name']
    except KeyError as e:
        print("{} not present in host number {}.\nWill not configure the host".format(e, host_number))
        return
    
    try:
        host['placeholders']['name'] = {'type': 'constant', 'value': host_name}
    except (KeyError, TypeError):
        host['placeholders'] = {}
        host['placeholders']['name'] = {'type': 'constant', 'value': host_name}

    count = host['count'] if 'count' in host else 1

    if 'type' not in host:
        host['type'] = "unknown"

    config_dir = "configs-" + host['type']

    with contextlib.suppress(FileExistsError):
        os.mkdir(config_dir)
    
    if count < 1:
        print ("count cannot be less than 1. Skipping host {} at index {}".format(host_name, host_number))
        return

    if not isinstance(host['configs'],list):
        print ("configs in host {} at index {} should be a list.\nSkipping host!!".format(host_name, host_number))
        return

    model_templates = [None] * len(host['configs'])
    placeholders = list()

    try:
        for config_index, config in enumerate(host['configs']):
            for template in config['templates']:
                if model_templates[config_index] is not None:
                    model_templates[config_index] = {**model_templates[config_index], **templates[template]}
                else:
                    model_templates[config_index] = {**templates[template]}
            if config['placeholders'] is None:
                config['placeholders'] = {}
            placeholders.append({ **host['placeholders'], **config['placeholders']})

            matches = find_placeholders(str(model_templates[config_index]))
            for match in matches:
                if match[2:-1] not in placeholders[config_index]:
                    print("{} placeholder is not present in the {} at index {} in config index {}.\nSkipping host".format(match, host_name, host_number, config_index + 1))
                    return
            
    except KeyError as e:
        print("{} is not present in the {} at index {}".format(e, host_name, host_number))
        return

    try:
        host_list = []
        with concurrent.futures.ThreadPoolExecutor() as executor:
            configs = [executor.submit(generate_config, deployment_name, count_num, host_number, host, model_templates, placeholders, config_dir=config_dir)
                for count_num in range(count)]

            for config in concurrent.futures.as_completed(configs):
                host_list.append(config.result())
        
        print()

    except KeyError as e:
        print("{} is not present in the {} at index {}".format(e, host_name, host_number))
        return

    if host['type'] == 'kvm':
        seeds = [os.path.join(config_dir, host_uniqueName + '.seed') for host_uniqueName in host_list]
        subprocess.run(['scp', *seeds, host['target']['host'] + ':' + host['target']['location']])

def apply(argv):
    helpMessage = "Usage:\n " + os.path.basename(__file__) + """ apply -f <deployment file> -t <template_file file>

  Options:
      -f, --file      Specify deployment file
      -t, --template_file  Specify template_file file
  
    -h: Display this message and exit"""
    
    inputFile = ""
    inputFileExists = False
    try:
        opts, args = getopt.getopt(argv, "hf:", ["file="])
    except getopt.GetoptError:
        print (helpMessage)
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-f", "--file"):
            inputFile = arg
            if os.path.exists(inputFile) == False:
                print ("Input file ", inputFile, " not found")
                return
            else:
                inputFileExists = True
        else:
            print (helpMessage)
            return

    if inputFileExists == False:
        print ("Please provide an Input file")
        print (helpMessage)
        return

    with open(inputFile, 'r') as stream:
        try:
            vm_deploy = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
            return
    
    try:
        deploy_dir = "./configs-" + vm_deploy['name']
    except KeyError as e:
        print("{} not present in deployment.\nTerminating code...".format(e))
        return

    templates = None
    templates = {}

    try:
        for template_file in vm_deploy['includes']:
            try:
                with open(template_file, 'r') as stream:
                    try:
                        templates = {**templates, **yaml.safe_load(stream)}
                    except yaml.YAMLError as exc:
                        print(exc)
                        return
            except FileNotFoundError as e:
                print(e)
                print("Terminating code")
                return
    except KeyError as e:
        print("no {} key found in the deployment file.\nTerminating program".format(e))
        return

    with contextlib.suppress(FileNotFoundError):
        shutil.rmtree(deploy_dir)
    os.mkdir(deploy_dir)

    with working_directory(deploy_dir):
        with concurrent.futures.ThreadPoolExecutor() as executor:
            hosts = [executor.submit(generate_host, host_index, host, templates, vm_deploy['name']) for host_index, host in enumerate(vm_deploy['hosts'])]
            for host in concurrent.futures.as_completed(hosts):
                host.result()


def main(argv):
    helpMessage = "Usage:\n " + os.path.basename(__file__) + """ <command> <[Options]>

  Commands:
      apply   Apply a configuration
  
    -h: Display this message and exit"""
    # if len(sys.argv) == 1:
    #     print (helpMessage)
    #     return

    if argv[0] == "apply":
        apply(argv[1:])
    else:
        print (helpMessage)
        return
# if __name__ == "__main__":
#     main(sys.argv[1:])


#arguments = ['apply', '-h']

arguments = ['apply', '-f', 'vm-deploy.yaml']

main(arguments)
