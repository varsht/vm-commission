#!/usr/bin/env bash

virt-install --name test-vm-1 \
    --virt-type kvm --memory 2048 --vcpus 2 \
    --boot hd,menu=on \
    --disk path=vm-configs/test-vm-1.qcow2,device=cdrom \
    --disk path=/home/varsh/virtual-machines/test-vm-1.qcow2,device=disk,size=10,format=qcow2,backing_store=/home/varsh/virtual-machines/focal-server-cloudimg-amd64.img,backing_format=qcow2 \
    --description "Test VM" \
    --os-type=Linux --os-variant=ubuntu20.04 \
    --network bridge:bridge0 \
    --network bridge:bridge1 \
    --graphics none \
    --noautoconsole

