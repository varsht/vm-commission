#!/usr/bin/env bash

while IFS= read -r host; do
    cloud-localds -v --network-config="$1/network-config-$host.cfg" "$1/$host.seed" "$1/cloud-config-$host.cfg"
done < <(tr ' ' '\n' <"$2")
